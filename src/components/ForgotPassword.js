import React from 'react'
import './Form.css'
import myInput from './field/index'
import {connect} from 'react-redux'
import {Link, withRouter} from 'react-router-dom'
import {Field, reduxForm} from "redux-form";
import {email, required} from "../utils/validate";
import {forgotPass} from "../actions/UserAction";
import {Button} from 'reactstrap';

const wrapWithHistory = (onSubmit, handleSubmit) => handleSubmit(v => onSubmit(v));

const Password = ({handleSubmit, onSubmit, valid}) => (
    <div className='auth'>
        <form onSubmit={wrapWithHistory(onSubmit, handleSubmit)} className="form">
            <div>Забыли пароль</div>
            <Field
                name="login"
                component={myInput}
                type="text"
                placeholder="Email"
                validate={[required, email]}
            />
            <Button type="submit" label="submit" disabled={!valid}>
                Отправить ссылку
            </Button>
            <div className='link'>
                <Link to='/login'>Войти</Link>
                <Link to='/registration'>Регистрация</Link>
            </div>
        </form>
    </div>
);

const ForgotPassword = reduxForm({
    form: 'forgotpass',
})(Password);

export default connect(null, {onSubmit: forgotPass})(withRouter(ForgotPassword))