import React from 'react'
import { GoogleLogin } from 'react-google-login'
import {connect} from 'react-redux'
import {authWithGoogle} from "../actions/UserAction";
import {withRouter} from 'react-router-dom'

const wrapWithHistory = (auth,history) => response => auth(response,history)

const AuthWithGoogle = ({authWithGoogle, history}) => (
    <GoogleLogin onSuccess={wrapWithHistory(authWithGoogle,history)} className='button' buttonText='Google' onFailure={console.error} clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}/>
)


export default connect(null,{authWithGoogle})(withRouter(AuthWithGoogle))