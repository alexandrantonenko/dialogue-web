import React from 'react'
import './Form.css'
import myInput from './field/index'
import {connect} from 'react-redux'
import queryString from 'query-string'
import { Link, withRouter} from 'react-router-dom'
import {Field, reduxForm} from "redux-form";
import {password, required, passwords} from "../utils/validate";
import {updatePass} from "../actions/UserAction";
import { Button } from 'reactstrap';

const wrapWithHistory = (search,onSubmit, handleSubmit,history) => handleSubmit(v => onSubmit(v,queryString.parse(search),history));

const Update = ({location:{search},handleSubmit,onSubmit,valid,history}) =>  (
    <div className='auth'>
        <form onSubmit={wrapWithHistory(search,onSubmit,handleSubmit,history)} className="form">
            <div>Обновить пароль</div>
            <Field
                name="password"
                component={myInput}
                type="password"
                placeholder="Введите пароль"
                validate={[required, password, passwords]}
            />
            <Field
                name="password_confirmation"
                component={myInput}
                type="password"
                placeholder="Повторите пароль"
                validate={required}
            />
            <Button type="submit" label="submit" disabled={!valid}>
                Обновить пароль
            </Button>
            <div className='link'>
                <Link to='/login'>Войти</Link>
                <Link to='/registration'>Регистрация</Link>
            </div>
        </form>
    </div>
)

const UpdatePassword = reduxForm({
    form: 'updatepass',
})(Update)

export default connect(null,{onSubmit:updatePass})(withRouter(UpdatePassword))