import React from 'react'
import VkAuth from 'react-vk-auth'
import {connect} from 'react-redux'
import {authWithVk} from "../actions/UserAction";
import {withRouter} from 'react-router-dom'
import './socialButton.css'

const wrapWithHistory = (auth,history) => response => auth(response,history)

const AuthWithVk = ({authWithVk,history}) => (
    <VkAuth apiId = {process.env.REACT_APP_VK_API} className='button' callback = {wrapWithHistory(authWithVk,history)} type='button' >VK</VkAuth>
)

export default connect(null,{authWithVk})(withRouter(AuthWithVk))