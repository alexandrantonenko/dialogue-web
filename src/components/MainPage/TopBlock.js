import React from 'react'
import {Button} from 'reactstrap'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'
import {logout} from "../../actions/UserAction";
import {STATUSES} from "../../utils/const";

const logoutWrap = (logout, history) => () => logout(history);

const TopBlock = ({logout, history,user,currentStatus, totalCount}) => {
    return (
        <div className='top-block'>
            {currentStatus === STATUSES.NEW ? <span className='line_clients'>A line of clients: {totalCount} </span>: null}
            <span className='user_login'>{user.login}</span>
            <Button size="sm" className='logout-button' onClick={logoutWrap(logout, history)}>Logout</Button>
        </div>
    )
};

export default connect((({user:{user}, dialogs:{currentStatus, totalCount}})=>({user,currentStatus, totalCount})), {logout})(withRouter(TopBlock))