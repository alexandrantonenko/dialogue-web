import React, {Component} from 'react'
import connect from "react-redux/es/connect/connect";
import {getDialogue} from "../../actions/DialogueAction";
import {STATUSES} from "../../utils/const";

class LeftBlock extends Component {

    handleClick = (typeAction) => () => {
        const {getDialogue} = this.props;
        switch (typeAction) {
            case STATUSES.NEW:
                getDialogue(STATUSES.NEW)
                break;
            case STATUSES.ACTIVE:
                getDialogue(STATUSES.ACTIVE)
                break;
            case STATUSES.SAVE:
                getDialogue(STATUSES.SAVE)
                break;
            case STATUSES.CLOSE:
                getDialogue(STATUSES.CLOSE)
                break;
            default:
                getDialogue()
        }
    }

    render() {
        return (
            <div className='left-blocks'>
                <div className='new block' onClick={this.handleClick(STATUSES.NEW)}>New Requests</div>
                <div className='active block' onClick={this.handleClick(STATUSES.ACTIVE)}>Active</div>
                <div className='closed block' onClick={this.handleClick(STATUSES.CLOSE)}>Completed</div>
                <div className='save block' onClick={this.handleClick(STATUSES.SAVE)}>Saved</div>
            </div>
        )
    }
}


export default connect(null, {getDialogue})(LeftBlock)