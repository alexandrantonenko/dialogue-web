import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getDialogue, getDialog } from '../../actions/DialogueAction'
import { withRouter } from 'react-router-dom'
import SearchInput from '../field/searchInput'
import OneDialog from './OneDialog'
import InfiniteScroll from 'react-infinite-scroll-component'

class Dialogue extends Component {
  componentDidMount() {
    this.props.getDialogue()
  }

  changeDialog = () => {
    const { view, getDialog } = this.props
    if (view) {
      getDialog()
    }
  }

  render() {
    const { dialogs, loading } = this.props
    return (
      <div className="dialogue">
        <SearchInput />
        <div className="dialogs">
          <InfiniteScroll
            dataLength={dialogs && dialogs.length}
            next={this.changeDialog}
            hasMore={!loading}
            scrollThreshold={0.95}
            height={650}
          >
            {dialogs &&
              !loading &&
              dialogs.map(v => <OneDialog dialog={v} key={v.created_at} />)}
          </InfiniteScroll>
        </div>
      </div>
    )
  }
}

export default connect(
  ({ dialogs: { dialogs, view, loading } }) => ({ dialogs, view, loading }),
  {
    getDialogue,
    getDialog,
  }
)(withRouter(Dialogue))
