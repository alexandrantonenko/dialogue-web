import React, { Component } from 'react'
import moment from 'moment'
import InfiniteScroll from 'react-infinite-scroller'
import connect from 'react-redux/es/connect/connect'
import { getMessage, sendMessage } from '../../../actions/MessagesAction'
import 'moment/locale/ru'
import './MessageBox.css'

class MessagesBox extends Component {
  state = {
    hasMore: false,
    isReverse: true,
    scrollTo: 0,
    search: false,
    oldestMessage: '',
    scrollToMessage: '',
    scrollToInitMessage: '',
  }
  constructor(props) {
    super(props)

    this.boxScrollRef = null
    this.setBoxScrollRef = e => {
      this.boxScrollRef = e
    }
  }
  componentDidMount() {
    const { messages, dialogs } = this.props
    const { message } = messages
    this.setState({
      oldestMessage: message[0].id,
      scrollToMessage: message[message.length - 1].id,
      scrollToInitMessage: dialogs.scrollToMessage,
    })
    let scroll = 0
    this.boxScrollRef.addEventListener('scroll', this.onScroll(scroll), false)
    setTimeout(() => {
      this.state.scrollToInitMessage
        ? this.scrollToInitElement(this.state.scrollToInitMessage)
        : this.props.scrollToBottom()
    }, 0)
  }
  componentDidUpdate() {
    this.searchMessages()
  }
  static getDerivedStateFromProps(props, state) {
    const { messages } = props
    const { message } = messages
    if (message[0].id !== state.oldestMessage) {
      return { oldestMessage: message[0].id }
    }
    if (messages.messagesFound.length > 0) {
      return { search: true }
    }
    return { search: false }
  }

  onScroll = scroll => () => {
    if (
      !this.state.hasMore &&
      this.boxScrollRef.scrollHeight !== this.boxScrollRef.offsetHeight
    ) {
      this.setState({ hasMore: true })
    }
    // else if (this.boxScrollRef.scrollTop > scroll && this.state.isReverse) {
    //   this.setState({ isReverse: false })
    // }
    else if (this.boxScrollRef.scrollTop < scroll && !this.state.isReverse) {
      this.setState({ isReverse: true })
    }
    scroll = this.boxScrollRef.scrollTop
  }

  getMessage = () => {
    const { getMessage, dialogs, messages } = this.props
    if (!messages.loading) {
      getMessage(dialogs.dialog_open)
    }
  }

  searchMessages = () => {
    const { messages, dialogs, getMessage } = this.props
    const { messagesFound } = messages
    if (messagesFound[this.state.scrollTo]) {
      if (this.state.oldestMessage >= messagesFound[this.state.scrollTo].id) {
        getMessage(dialogs.dialog_open)
      } else {
        document
          .getElementById(messagesFound[this.state.scrollTo].id)
          .classList.remove('found_message')
        this.scrollToElement(messagesFound[this.state.scrollTo].id)
      }
    }
  }
  scrollToElement = id => {
    const element = document.getElementById(id)
    element.classList.add('found_message')
    element.scrollIntoView({
      behavior: 'smooth',
    })
  }

  scrollToInitElement = id => {
    const element = document.getElementById(id)
    element.classList.add('found_message')
    element.scrollIntoView({
      behavior: 'instant',
    })
  }

  componentWillUnmount() {
    this.boxScrollRef.removeEventListener('scroll', this.onScroll, false)
  }

  turnMessage = direction => () => {
    switch (direction) {
      case 'up':
        return this.setState({ scrollTo: this.state.scrollTo + 1 })
      case 'down':
        return this.setState({ scrollTo: this.state.scrollTo - 1 })
      default:
        return console.log('******')
    }
  }

  render() {
    const { messages, divBottom } = this.props
    return (
      <div className="messages_box" ref={this.setBoxScrollRef}>
        {this.state.search && (
          <div className="btn_turn">
            <button
              className="btn btn-secondary"
              onClick={this.turnMessage('up')}
              disabled={
                this.state.scrollTo === messages.messagesFound.length - 1
              }
            >
              ▲
            </button>
            <button
              className="btn btn-secondary"
              onClick={this.turnMessage('down')}
              disabled={this.state.scrollTo === 0}
            >
              ▼
            </button>
          </div>
        )}

        <InfiniteScroll
          pageStart={0}
          className="scroll_box"
          loadMore={this.getMessage}
          hasMore={this.state.hasMore}
          isReverse={this.state.isReverse}
          useWindow={false}
          threshold={300}
        >
          {messages.message &&
            !messages.loading &&
            messages.message.map(item => {
              return (
                <div
                  className={
                    item.operator_sent ? 'operator_message' : 'client_message'
                  }
                  key={item.id}
                  id={item.id}
                  ref={item.id === this.state.scrollToMessage && divBottom}
                >
                  <div className="text_message">{item.text}</div>
                  <div className="date_message">
                    {moment(item.created_at).calendar()}
                  </div>
                </div>
              )
            })}
        </InfiniteScroll>
      </div>
    )
  }
}
export default connect(
  ({ messages, dialogs }) => ({ messages, dialogs }),
  {
    sendMessage,
    getMessage,
  }
)(MessagesBox)
