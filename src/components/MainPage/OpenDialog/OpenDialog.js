import React, { Component } from 'react'
import SearchInput from '../../field/searchInput'
import connect from 'react-redux/es/connect/connect'
import { closeDialog } from '../../../actions/DialogueAction'
import { getAnswers } from '../../../actions/AnswerAction'
import {
  getMessages,
  sendMessage,
  getMessage,
  clearMessages,
} from '../../../actions/MessagesAction'
import Select from 'react-select'
import MessagesBox from './MessagesBox'
import moment from 'moment'
import 'moment/locale/ru'
import './OpenDialog.css'

moment.locale('ru')

class OpenDialog extends Component {
  state = {
    message: null,
  }

  constructor(props) {
    super(props)

    this.divScrollRef = null

    this.setDivScrollRef = element => {
      this.divScrollRef = element
    }
  }

  handleChange = message => {
    this.setState({ message: message })
  }

  handleChangeText = e => {
    this.setState({
      message: e.currentTarget.value,
    })
  }

  scrollToBottom = () => {
    return this.divScrollRef.scrollIntoView({ behavior: 'instant' })
  }

  componentDidMount() {
    const { getAnswers, getMessages, dialogs } = this.props
    getMessages(dialogs.dialog_open)
    getAnswers()
  }

  closeDialog = () => {
    const { closeDialog, clearMessages } = this.props
    closeDialog()
    clearMessages()
  }

  componentWillUnmount() {
    const { clearMessages } = this.props
    clearMessages()
  }

  sendMessage = e => {
    const { sendMessage } = this.props
    const { message } = this.state
    e.preventDefault()
    if (typeof message == 'string') {
      sendMessage(message, () => {
        this.scrollToBottom()
      })
    } else {
      sendMessage(message.label, () => {
        this.scrollToBottom()
      })
    }
    this.setState({ message: null })
    e.target.reset()
  }

  getMessage = () => {
    const { getMessage, dialogs, messages } = this.props
    if (!messages.loading) {
      getMessage(dialogs.dialog_open, messages.message.length)
    }
  }

  render() {
    const { answers, messages } = this.props
    const { message } = this.state
    return (
      <div className="open_dialog">
        <SearchInput />
        <div className="messages">
          <button
            className="butt_close btn btn-secondary"
            onClick={this.closeDialog}
          >
            x
          </button>
          {messages && !!messages.message.length && (
            <MessagesBox
              messages={messages}
              divBottom={this.setDivScrollRef}
              scrollToBottom={this.scrollToBottom}
            />
          )}
          <div className="dialog_time">Dialog time</div>
          <div className="submission">
            <form className="submission_form" onSubmit={this.sendMessage}>
              <textarea
                className="enter_text"
                type="text"
                onChange={this.handleChangeText}
                style={{ resize: 'none' }}
                placeholder="Enter answer..."
              />
              <div className="options">
                <button
                  type="button"
                  className="btn btn-secondary butt_setting"
                >
                  Setting
                </button>
                <Select
                  className="select_text"
                  placeholder="Select"
                  value={message}
                  onChange={this.handleChange}
                  options={answers.answers}
                />
              </div>
              <button
                type="submit"
                className="btn btn-secondary button_send"
                disabled={!message}
              >
                Send
              </button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  ({ answers, messages, dialogs }) => ({ answers, messages, dialogs }),
  {
    closeDialog,
    getAnswers,
    getMessages,
    sendMessage,
    getMessage,
    clearMessages,
  }
)(OpenDialog)
