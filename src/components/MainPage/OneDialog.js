import React, { Component } from 'react'
import moment from 'moment'
import 'moment/locale/ru'
import * as _ from 'lodash'
import connect from 'react-redux/es/connect/connect'
import {
  changeStateDialog,
  getDialogue,
  deleteDialog,
  openDialog,
} from '../../actions/DialogueAction'
import { STATUSES } from '../../utils/const'

moment.locale('ru')

class OneDialog extends Component {
  handleClick = (typeAction, id, message_id) => () => {
    const {
      getDialogue,
      changeStateDialog,
      dialogs,
      totalCount,
      deleteDialog,
      openDialog,
    } = this.props
    switch (typeAction) {
      case STATUSES.NEW:
        changeStateDialog(id, STATUSES.NEW)
        break
      case STATUSES.ACTIVE:
        changeStateDialog(id, STATUSES.ACTIVE)
        openDialog(id)
        break
      case STATUSES.SAVE:
        changeStateDialog(id, STATUSES.SAVE)
        break
      case STATUSES.CLOSE:
        changeStateDialog(id, STATUSES.CLOSE)
        break
      case 'OPEN':
        openDialog(id, message_id)
        break
      case 'DELETE':
        deleteDialog(id)
        break
      default:
        getDialogue()
    }
    if (totalCount > 7 && dialogs.length < 8) {
      getDialogue()
    }
  }

  renderStar = idx => {
    const { rating } = this.props.dialog
    if (idx < rating) {
      return <span key={idx}>&#9733;</span>
    } else {
      return <span key={idx}>&#9734;</span>
    }
  }

  render() {
    const { dialog } = this.props
    const { login, created_at, id, status_id, text, message_id } = dialog
    return (
      <div>
        <div className="message" key={created_at}>
          <div className="client">{login}</div>
          <div className="text">
            <p>{text}</p>
          </div>
          <div className="date">{moment(created_at).fromNow()}</div>
          {status_id === STATUSES.NEW && (
            <button
              className="btn btn-success btn-click btn-open"
              onClick={this.handleClick(STATUSES.ACTIVE, id)}
            >
              Open
            </button>
          )}
          {(status_id === STATUSES.ACTIVE || status_id === STATUSES.CLOSE) && (
            <button
              className="btn btn-success btn-click btn-save"
              onClick={this.handleClick(STATUSES.SAVE, id)}
            >
              Save
            </button>
          )}
          {status_id === STATUSES.ACTIVE && (
            <button
              className="btn btn-success btn-click btn-continue"
              onClick={this.handleClick('OPEN', id, message_id)}
            >
              Continue
            </button>
          )}
          {status_id === STATUSES.SAVE && (
            <button
              className="btn btn-success btn-click btn-delete"
              onClick={this.handleClick('DELETE', id)}
            >
              Delete
            </button>
          )}
          {(status_id === STATUSES.SAVE || status_id === STATUSES.CLOSE) && (
            <div className="rating">{_.times(5, this.renderStar)}</div>
          )}
        </div>
      </div>
    )
  }
}

export default connect(
  ({ dialogs: { view, totalCount, dialogs } }) => ({
    view,
    totalCount,
    dialogs,
  }),
  {
    changeStateDialog,
    getDialogue,
    deleteDialog,
    openDialog,
  }
)(OneDialog)
