import React, { Component } from 'react'
import './MainPage.css'
import './TopBlock'
import { connect } from 'react-redux'
import TopBlock from './TopBlock'
import { withRouter } from 'react-router-dom'
import LeftBlock from './LeftBlock'
import Dialogue from './Dialogue'
import OpenDialog from './OpenDialog/OpenDialog'
import { validateToken } from '../../actions/UserAction'

class MainPage extends Component {
  componentDidMount() {
    const { validateToken } = this.props
    validateToken()
  }

  isNumeric = id => {
    return isNaN(parseFloat(id)) && isFinite(id)
  }

  render() {
    const { dialog_open } = this.props.dialogs
    return (
      <div className="main-page">
        {!this.isNumeric(dialog_open) ? <OpenDialog /> : <Dialogue />}
        <TopBlock />
        <LeftBlock />
      </div>
    )
  }
}

export default connect(
  ({ dialogs }) => ({ dialogs }),
  { validateToken }
)(withRouter(MainPage))
