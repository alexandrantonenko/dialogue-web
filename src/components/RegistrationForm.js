import React from 'react'
import { Field, reduxForm } from 'redux-form'
import './Form.css'
import myInput from './field/index'
import {required, email, password, passwords} from '../utils/validate'
import {Link, withRouter} from 'react-router-dom'
import {connect} from 'react-redux'
import AuthWithGoogle from './google'
import AuthWithVk from './vk'
import {register} from "../actions/UserAction";
import { Button, Form } from 'reactstrap';

const wrapWithHistory = (history, onSubmit, handleSubmit) => handleSubmit(v => onSubmit(v, history));

const Registration = ({handleSubmit,onSubmit,valid,history}) => (
    <div className='auth'>
        <Form onSubmit={wrapWithHistory(history,onSubmit,handleSubmit)} className="form">
            <div>Регистрация</div>
            <Field
                name="login"
                component={myInput}
                type="text"
                placeholder="Email"
                validate={[required, email]}
            />
            <Field
                name="password"
                component={myInput}
                type="password"
                placeholder="Password"
                validate={[required,password, passwords]}
            />
            <Field
                name="password_confirmation"
                component={myInput}
                type="password"
                placeholder="Повторите пароль"
                validate={required}
            />
            <Button type="submit" label="submit" disabled={!valid}>
                Регистрация
            </Button>
            <div className='social'>
                <AuthWithGoogle />
                <AuthWithVk />
            </div>
            <div className='link'>
                <Link to='/login'>Войти</Link>
                <Link to='/forgotpass'>Забыли пароль</Link>
            </div>
        </Form>
    </div>
)



const RegistrationForm = reduxForm({
    form: 'register',
    // validate,
})(Registration)

export default connect(null,{onSubmit:register})(withRouter(RegistrationForm))