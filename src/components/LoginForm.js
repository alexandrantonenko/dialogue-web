import React from 'react'
import {Field, reduxForm} from 'redux-form'
import './Form.css'
import myInput from './field/index'
import {required, email} from '../utils/validate'
import {connect} from 'react-redux'
import AuthWithGoogle from './google'
import AuthWithVk from './vk'
import {Link, withRouter} from 'react-router-dom'
import {submitForm} from '../actions/UserAction'
import {Button, Form} from 'reactstrap';


const wrapWithHistory = (onSubmit, handleSubmit, history) => handleSubmit(v => onSubmit(v, history));

const Login = ({handleSubmit, onSubmit, valid, history}) => (
    <div className='auth'>
        <Form onSubmit={wrapWithHistory(onSubmit, handleSubmit, history)} className="form">
            <div>Авторизация</div>
            <Field
                name="login"
                component={myInput}
                type="text"
                placeholder="Email"
                validate={[required, email]}
            />
            <Field
                name="password"
                component={myInput}
                type="password"
                placeholder="Password"
                validate={required}
            />
            <Button type="submit" label="submit" disabled={!valid}>
                Войти
            </Button>
            <div className='social'>
                <AuthWithGoogle/>
                <AuthWithVk/>
            </div>
            <div className='link'>
                <Link to='/registration'>Зарегистрироваться</Link>
                <Link to='/forgotpass'>Забыли пароль?</Link>
            </div>
        </Form>
    </div>
);


const LoginForm = reduxForm({
    form: 'login',
})(Login);

export default connect(null, {onSubmit: submitForm})(withRouter(LoginForm))
