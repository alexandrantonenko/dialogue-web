import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getDialogue, searchDialogue } from '../../actions/DialogueAction'
import {
  getMessages,
  searchMessages,
  clearSearch,
} from '../../actions/MessagesAction'
import _ from 'lodash'

class SearchInput extends Component {
  constructor(props) {
    super(props)
    this.myRef = React.createRef()
  }

  searchBy = arg => () => {
    switch (arg) {
      case 'DIALOGS':
        return this.searchByDialogs()
      case 'MESSAGES':
        return this.searchByMessages()
      default:
        console.log('**')
    }
  }
  searchByDialogs = () => {
    const { searchDialogue, getDialogue } = this.props
    if (this.myRef.current.value.length > 0) {
      searchDialogue(this.myRef.current.value)
    } else {
      getDialogue()
    }
  }
  searchByMessages = () => {
    const { searchMessages, clearSearch } = this.props
    if (this.myRef.current.value.length > 0) {
      searchMessages(this.myRef.current.value)
    } else {
      clearSearch()
    }
  }

  render() {
    const { dialogs } = this.props
    const search =
      typeof dialogs.dialog_open == 'string' ? 'DIALOGS' : 'MESSAGES'
    return (
      <div className="search">
        <div className="search-group" role="group" aria-label="Basic example">
          <input
            className="form-control"
            placeholder="Enter something..."
            onChange={_.debounce(this.searchBy(search), 300)}
            defaultValue=""
            ref={this.myRef}
          />
        </div>
      </div>
    )
  }
}

export default connect(
  ({ dialogs }) => ({ dialogs }),
  { getDialogue, searchDialogue, getMessages, searchMessages, clearSearch }
)(SearchInput)
