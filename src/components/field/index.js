import React, { Fragment } from 'react'
import { Input } from 'reactstrap';
import './index.css'


const myInput = props => {
  const { input, type, placeholder, meta } = props;

  return (
    <Fragment>
        <p className='title'>{placeholder}</p>
      <Input
        {...input}
        type={type}
        className={meta.error && meta.touched ? 'error' : undefined}
      />
        {meta.error &&
        meta.touched &&
        <p>
            {meta.error}
        </p>}
    </Fragment>
  )
};


export default myInput
