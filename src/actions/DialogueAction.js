import {
  SEARCHMODE,
  GETDIALOGUE,
  GETDIALOGUEINIT,
  CHANGESTATEDIALOG,
  VIEWMODE,
  SEARCH,
  GETDIALOG,
  STATUSES,
  DIALOGDELETE,
  OPENDIALOG,
  CLOSEDIALOG,
} from '../utils/const'

export const getDialogue = e => async (dispatch, getState) => {
  if (getState().user.user.operator) {
    dispatch({ type: GETDIALOGUEINIT })
    const status = e !== undefined ? e : getState().dialogs.currentStatus
    const operator = status === STATUSES.NEW ? '' : getState().user.user.id
    const response = await fetch(
      `http://localhost:3000/dialog/get/?status=${status}&operator_id=${operator}`,
      {
        method: 'GET',
      }
    )
    if (response.ok) {
      dispatch({ type: VIEWMODE })
      const dialogs = await response.json()

      dispatch({
        type: GETDIALOGUE,
        payload: [dialogs.rows, status, dialogs.totalCount],
      })
    }
  }
}

export const getDialog = () => async (dispatch, getState) => {
  const { currentStatus, dialogs } = getState().dialogs
  const length = dialogs.length
  const operator = currentStatus === STATUSES.NEW ? '' : getState().user.user.id
  const response = await fetch(
    `http://localhost:3000/dialog/get/?status=${currentStatus}&length=${length}&operator_id=${operator}`,
    {
      method: 'GET',
    }
  )
  const dialog = await response.json()
  if (response.ok && dialog.rows.length !== 0) {
    dispatch({ type: GETDIALOG, payload: dialog.rows })
  }
}

export const searchDialogue = value => async (dispatch, getState) => {
  const { currentStatus } = getState().dialogs
  const operator = currentStatus === STATUSES.NEW ? '' : getState().user.user.id
  const response = await fetch(
    `http://localhost:3000/dialog/search/?word=${value}&status=${currentStatus}&operator_id=${operator}`,
    {
      method: 'GET',
    }
  )
  if (response.ok) {
    const dialog = await response.json()
    dispatch({ type: SEARCHMODE })
    dispatch({ type: SEARCH, payload: [dialog.rows, currentStatus] })
  }
}

export const deleteDialog = id => async dispatch => {
  const response = await fetch(
    `http://localhost:3000/dialog/delete/?id=${id}`,
    {
      method: 'GET',
    }
  )
  if (response.ok) {
    dispatch({ type: DIALOGDELETE, payload: id })
  }
}

export const changeStateDialog = (id, status) => async (dispatch, getState) => {
  const operator = status === STATUSES.NEW ? '' : getState().user.user.id
  const response = await fetch(
    `http://localhost:3000/dialog/change/?id=${id}&status=${status}&operator_id=${operator}`,
    {
      method: 'POST',
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
      },
    }
  )
  if (response.ok) {
    dispatch({ type: CHANGESTATEDIALOG, payload: id })
  }
}

export const subscribeToTopic = token => async () => {
  await fetch(`http://localhost:3000/dialog/token/?token=${token}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'text/plain',
      'Access-Control-Allow-Origin': '*',
    },
  })
}

export const openDialog = (id, message_id) => async dispatch => {
  dispatch({ type: OPENDIALOG, payload: [id, message_id] })
}

export const closeDialog = () => async dispatch => {
  dispatch({ type: CLOSEDIALOG })
}
