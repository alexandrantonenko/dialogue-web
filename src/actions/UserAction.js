import {SubmissionError} from 'redux-form'
import {toast} from "react-toastify";

export const AUTHENTICATED = 'AUTHENTICATED';
export const UNAUTHENTICATED = 'UNAUTHENTICATED';

export const headersNormalize = (token, access_token) => {
    const headers = {"Content-Type": "application/json"};
    if (token) {
        headers.Authorization = `Bearer ${token}`;
    } else if (access_token) {
        headers.access_token = access_token;
    }
    return headers;
};

export const register = (values, history) => async dispatch => {
    const res = await fetch(`http://localhost:3000/users/username?login=${values.login}`, {
        method: "GET",
        headers: headersNormalize()
    });
    if (res.ok) {
        const response = await fetch("http://localhost:3000/auth/sign_up", {
            method: "POST",
            headers: headersNormalize(),
            body: JSON.stringify(values)
        });
        if (response.ok) {
            dispatch({type: AUTHENTICATED, payload: await response.json()});
            history.push('/')
        }
    } else {
        throw new SubmissionError({login: "Этот email уже зарегистрирован"});
    }
};

export const forgotPass = (values) => async () => {
    const response = await fetch(`http://localhost:3000/auth/forgot_password?login=${values.login}`, {
        headers: headersNormalize()
    });
    if (response.ok) {
        toast("Письмо со ссылкой отправлено");
    } else {
        throw new SubmissionError({login: "Проверьте введенные данные"});
    }
};

export const updatePass = (values, {token}, history) => async () => {
    const response = await fetch(`http://localhost:3000/auth/update_password?token=${token}`, {
        method: "POST",
        headers: headersNormalize(),
        body: JSON.stringify(values)
    });
    if (response.ok) {
        toast('Пароль обновлен');
        setTimeout(history.push('/'), 3000)
    } else {
        throw new SubmissionError({password: 'Проверьте введенные данные'})
    }
};


export const submitForm = (values, history) => async dispatch => {
    const response = await fetch('http://localhost:3000/auth/sign_in', {
        headers: headersNormalize(),
        method: 'POST',
        body: JSON.stringify(values)
    });

    if (response.ok) {
        const auth = await response.json();
        dispatch({type: AUTHENTICATED, payload: auth});
        history.push('/')
    } else {
        throw new SubmissionError({password: 'Неверный логин или пароль'})
    }
};

export const validateToken = () => async (dispatch, getState) => {
    const response = await fetch(`http://localhost:3000/tokens/validate?token=${getState().user.token}`, {});

    if (!response.ok) {
        dispatch({type: UNAUTHENTICATED});
    }
};

export const authWithGoogle = (googleResponse, history) => async (
    dispatch
) => {
    const response = await fetch('http://localhost:3000/auth/google', {
        method: 'GET',
        headers: headersNormalize(null, googleResponse.accessToken)
    });
    if (response.ok) {
        const auth = await response.json();
        dispatch({type: AUTHENTICATED, payload: auth});
        history.push('/')
    }
};

export const authWithVk = (vkResponse, history) => async (
    dispatch
) => {
    if (vkResponse.session) {
        const response = await fetch(`http://localhost:3000/auth/vkontakte?access_token=${vkResponse.session.sid}`, {
            method: 'GET',
            headers: headersNormalize()
        });
        if (response.ok) {
            const auth = await response.json();
            dispatch({type: AUTHENTICATED, payload: auth});
            history.push('/')
        }
    }
};

export const logout = () => dispatch => {
    dispatch({type: UNAUTHENTICATED})
};