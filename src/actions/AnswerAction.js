import {GETANSWERS} from "../utils/const";

export const getAnswers = () => async dispatch => {
    const response = await fetch('http://localhost:3000/answers/get', {
        method: "GET"
    })
    const answers = await response.json()
    if (response.ok) {
        dispatch({type: GETANSWERS, payload:answers})
    }
}
