import {
  GETMESSAGES,
  MESSAGESENT,
  GETMESSAGE,
  CLEARMESSAGES,
  GETMESSAGESINIT,
  DELETEMESSAGES,
  SEARCHMESSAGES,
  CLEARSEARCH,
} from '../utils/const'

export const getMessages = id => async (dispatch, getState) => {
  const message_id = getState().dialogs.scrollToMessage
  dispatch({ type: GETMESSAGESINIT })
  const response = await fetch(
    `http://localhost:3000/messages/get/?id=${id}&message_id=${message_id}`,
    {
      method: 'GET',
    }
  )
  if (response.ok) {
    const messages = await response.json()
    dispatch({ type: GETMESSAGES, payload: messages.message.rows })
  }
}

export const sendMessage = (text, scroll) => async (dispatch, getState) => {
  const data = {
    login: getState().user.user.login,
    id: getState().dialogs.dialog_open,
    text: text,
  }
  const response = await fetch('http://localhost:3000/messages/send', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data),
  })
  if (response.ok) {
    dispatch({ type: MESSAGESENT, payload: await response.json() })
    // const int = setInterval(() => {
    scroll()
    // }, 100)

    // setTimeout(() => {
    //   clearInterval(int)
    // }, 700)
  }
}

export const getMessage = id => async (dispatch, getState) => {
  const { messages } = getState()
  const { message } = messages
  const time = message[0].created_at
  const response = await fetch(
    `http://localhost:3000/messages/get/?id=${id}&time=${time}`,
    {
      method: 'GET',
    }
  )
  const reseivedMessages = await response.json()
  if (response.ok && reseivedMessages.message.rows.length !== 0) {
    dispatch({
      type: GETMESSAGE,
      payload: reseivedMessages.message.rows,
    })
    // if (getState().messages.message.length > 90 && reverse) {
    //   dispatch({ type: DELETEMESSAGES })
    // }
  }
}

export const searchMessages = words => async (dispatch, getState) => {
  const response = await fetch(
    `http://localhost:3000/messages/search/?id=${
      getState().dialogs.dialog_open
    }&words=${words}`
  )
  if (response.ok) {
    const id = await response.json()
    dispatch({ type: SEARCHMESSAGES, payload: id.messages_id.rows })
  }
}
export const clearSearch = () => async dispatch => {
  dispatch({ type: CLEARSEARCH })
}
export const clearMessages = () => async dispatch => {
  dispatch({ type: CLEARMESSAGES })
}
