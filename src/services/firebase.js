import * as React from "react";
import {connect} from 'react-redux'
import {getDialogue, subscribeToTopic} from "../actions/DialogueAction";
import firebase from "firebase/app"
import 'firebase/messaging'


class Firebase extends React.Component {
    componentDidMount() {
        const {subscribeToTopic, getDialogue} = this.props
        window.addEventListener("focus", this.handleFocus);
        firebase.initializeApp({
            apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
            authDomain: "ns-dialog.firebaseapp.com",
            databaseURL: "https://ns-dialog.firebaseio.com",
            projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
            storageBucket: "ns-dialog.appspot.com",
            messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID
        })
        const messaging = firebase.messaging()
        messaging.usePublicVapidKey("BJt4ndbtpE50ifHOyvjVBveJRv6cHwlkgwHZDSsASRrw9ib4Ku2nSz3YyWcfHHQu4Ah528f0iTDWX8CDvq_CEJ8")
        messaging.requestPermission().then(() => {
                return messaging.getToken()

            }
        ).then((token) => {
            subscribeToTopic(token)
        })
            .catch(error => {
                if (error.code === "messaging/permission-blocked") {
                    console.log("Please Unblock Notification Request Manually")
                } else {
                    console.log("Error Occurred", error);
                }
            });
        messaging.onMessage(payload => {
            getDialogue()

        })
    }

    componentWillUnmount() {
        window.removeEventListener('focus', this.handleFocus);
    }

    handleFocus = () => {
        this.props.getDialogue()
    }

    render() {
        return (
            <React.Fragment/>
        );
    }
}

export default connect(null, {getDialogue, subscribeToTopic})(Firebase);




