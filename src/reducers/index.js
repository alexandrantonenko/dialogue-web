import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { userReducer } from './user'
import { dialogReducer } from './dialogs'
import { answersReducer } from './answers'
import { messagesReducer } from './messages'

const rootReducer = combineReducers({
  user: userReducer,
  form: formReducer,
  dialogs: dialogReducer,
  messages: messagesReducer,
  answers: answersReducer,
})

export default rootReducer
