import {
  GETDIALOGUE,
  GETDIALOGUEINIT,
  CHANGESTATEDIALOG,
  GETDIALOG,
  SEARCHMODE,
  VIEWMODE,
  SEARCH,
  STATUSES,
  DIALOGDELETE,
  OPENDIALOG,
  CLOSEDIALOG,
} from '../utils/const'

const initialState = {
  currentStatus: STATUSES.NEW,
  view: true,
  dialog_open: '',
  totalCount: '',
  loading: false,
  scrollToMessage: '',
}

export function dialogReducer(state = initialState, action) {
  switch (action.type) {
    case GETDIALOGUEINIT:
      return {
        ...state,
        loading: true,
      }
    case GETDIALOGUE:
      return {
        ...state,
        dialogs: action.payload[0],
        currentStatus: action.payload[1],
        totalCount: action.payload[2],
        loading: false,
      }
    case CHANGESTATEDIALOG:
      return {
        ...state,
        dialogs: state.dialogs.filter(
          item => item.id !== Number(action.payload)
        ),
        totalCount: state.totalCount - 1,
      }
    case GETDIALOG:
      return { ...state, dialogs: state.dialogs.concat(action.payload) }
    case SEARCH:
      return {
        ...state,
        dialogs: action.payload[0],
        currentStatus: action.payload[1],
      }
    case DIALOGDELETE:
      return {
        ...state,
        dialogs: state.dialogs.filter(
          item => item.id !== Number(action.payload)
        ),
      }
    case SEARCHMODE:
      return { ...state, view: false }
    case VIEWMODE:
      return { ...state, view: true }
    case OPENDIALOG:
      return {
        ...state,
        dialog_open: action.payload[0],
        scrollToMessage: action.payload[1] || '',
      }
    case CLOSEDIALOG:
      return { ...state, dialog_open: '', scrollToMessage: '' }
    default:
      return state
  }
}

//
//
