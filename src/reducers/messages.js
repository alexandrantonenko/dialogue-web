import {
  GETMESSAGES,
  MESSAGESENT,
  GETMESSAGE,
  CLEARMESSAGES,
  GETMESSAGESINIT,
  DELETEMESSAGES,
  SEARCHMESSAGES,
  CLEARSEARCH,
} from '../utils/const'

const initialState = {
  loading: false,
  message: [],
  messagesFound: [],
}

export function messagesReducer(state = initialState, action) {
  switch (action.type) {
    case GETMESSAGESINIT:
      return { ...state, loading: true, message: [] }
    case GETMESSAGES:
      return {
        ...state,
        message: action.payload,
        loading: false,
      }
    case MESSAGESENT:
      return {
        ...state,
        message: state.message.concat(action.payload.message),
      }
    case SEARCHMESSAGES:
      return {
        ...state,
        messagesFound: action.payload,
      }
    case GETMESSAGE:
      return {
        ...state,
        message: action.payload.concat(state.message),
      }
    case CLEARSEARCH:
      return { ...state, messagesFound: [] }
    // case DELETEMESSAGES:
    //   return {
    //     ...state,
    //     message: state.message.slice(0, state.message.length - 30),
    //   }
    case CLEARMESSAGES:
      return { ...state, message: [], messagesFound: [] }
    default:
      return state
  }
}
