import {GETANSWERS} from "../utils/const";

const initialState = {
    answers: []
}

export function answersReducer(state = initialState, action) {
    switch (action.type) {
        case GETANSWERS:
            return {...state, ...action.payload}
        default:
            return state
    }
}