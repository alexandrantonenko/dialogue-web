import { AUTHENTICATED, UNAUTHENTICATED } from '../actions/UserAction'

const initialState = {
    user: {}
};

export function userReducer(state = initialState,action) {
    switch (action.type) {
        case AUTHENTICATED:
            return {...state, ...action.payload};
        case UNAUTHENTICATED:
            return {...initialState};
        default:
            return state
    }
}