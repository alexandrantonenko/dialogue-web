import {BrowserRouter, Route, Switch} from "react-router-dom";
import {connect} from "react-redux";
import LoginForm from "./components/LoginForm";
import RegistrationForm from "./components/RegistrationForm";
import ForgotPassword from "./components/ForgotPassword";
import UpdatePassword from "./components/UpdatePassword";
import MainPage from "./components/MainPage/MainPage";
import React from "react";

const Routes = ({token}) => {
    return (
        <BrowserRouter>
            <Switch>
                <Route path='/login' component={LoginForm}/>
                <Route path='/registration' component={RegistrationForm}/>
                <Route path='/forgotpass' component={ForgotPassword}/>
                <Route path='/update_password' component={UpdatePassword}/>
                <Route exact path='/' component={token ? MainPage: LoginForm}/>
            </Switch>
        </BrowserRouter>
    )
}

export default connect(({user: {token}}) => ({token}))(Routes)
