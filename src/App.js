import React from 'react'
import 'dotenv'
import './App.css'
import 'react-toastify/dist/ReactToastify.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import rootReducer from './reducers/index'
import { ToastContainer } from 'react-toastify'
import Routes from './Routes'
import storage from 'redux-persist/lib/storage'
import { persistStore, persistReducer } from 'redux-persist'
import Firebase from './services/firebase'

const persistConfig = {
  key: 'adwada',
  storage,
  whiteList: ['user'],
  blackList: ['messages', 'dialogs', 'answers'],
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

let store = createStore(
  persistedReducer,
  composeWithDevTools(applyMiddleware(thunk))
)
let persistor = persistStore(store)

const App = () => (
  <div className="main">
    <Provider store={store}>
      <PersistGate persistor={persistor} store={store} loading={null}>
        <Routes />
        <ToastContainer />
        <Firebase />
      </PersistGate>
    </Provider>
  </div>
)
export default App
