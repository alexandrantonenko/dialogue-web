const ServiceWorkerWebpackPlugin = require('serviceworker-webpack-plugin');
module.exports = {
    module: {
        plugins: [
            new ServiceWorkerWebpackPlugin({
                entry: path.join(__dirname, './firebase-messaging-sw.js'),
            })
        ]
    }

}