
export const required = value =>
    !value && "Пожалуйста, заполните поле";

export const email = value =>
    (value.match(/^\w+@\w+\.\w{2,4}$/i) || []).length !==1 && 'Некорректный email'

export const password = value =>
    !(value.match(/[0-9]/g,/[a-z]/g,/[A-Z]/g) && value.length>=8) && 'Пароль должен содержать цифру, буквы в нижнем и верхнем регистре и иметь длину не менее 8 знаков';

export const passwords = (value,values) => value!==values.password_confirmation && "Пароли не совпадают"