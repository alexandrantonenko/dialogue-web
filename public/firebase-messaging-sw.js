// if ('serviceWorker' in navigator) {
//     navigator.serviceWorker.register('../firebase-messaging-sw.js')
//         .then(function(registration) {
//             console.log('Registration successful, scope is:', registration.scope);
//         }).catch(function(err) {
//         console.log('Service worker registration failed, error:', err);
//     });
// }

importScripts("https://www.gstatic.com/firebasejs/5.5.9/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/5.5.9/firebase-messaging.js");
let config = {
    messagingSenderId: "911273393800"
};
firebase.initializeApp(config);
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(payload => {
    const title = payload.data.title;
    const options = {
        body: payload.data.body
    }
    return self.registration.showNotification(title, options);
})

self.addEventListener("notificationclick", function (event) {
    const promiseChain = clients
        .matchAll({
            type: "window",
            includeUncontrolled: true
        })
        .then(windowClients => {
            let matchingClient = null;
            for (let i = 0; i < windowClients.length; i++) {
                const windowClient = windowClients[i];
                if (windowClient.url === 'http://localhost:3001/') {
                    matchingClient = windowClient;
                    break;
                }
            }
            if (matchingClient) {
                return matchingClient.focus();


            } else {
                return clients.openWindow('/');
            }
        });
    event.waitUntil(promiseChain);
});